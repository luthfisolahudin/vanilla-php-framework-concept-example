{ sources ? import ./nix/sources.nix }:

let
  php_version = "8.2";
  node_version = "20";

  pkgs = import sources.nixpkgs { overlays = []; config = {}; };
  phps = (import sources.nix-phps).packages.${builtins.currentSystem};

  php = phps."php${builtins.replaceStrings ["."] [""] php_version}";
  nodejs = pkgs."nodejs_${node_version}";
  corepack = pkgs."corepack_${node_version}";
  niv = (import sources.niv {}).niv;
in

pkgs.mkShell {
  buildInputs = [
    (php.withExtensions ({ all, ... }: with all; [
        iconv
        mbstring
        pdo
        pdo_mysql
        session
    ]))

    php.packages.composer

    nodejs
    corepack

    niv
  ];
}
