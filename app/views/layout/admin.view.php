<?php

/**
 * @var Sys\View\ViewEngineInterface $v
 * @var string                       $slot
 */

$v->extends('layout/app');
$v->persist('title', "Admin: {$v->persist('title')}");

?>

<?= $slot; ?>
