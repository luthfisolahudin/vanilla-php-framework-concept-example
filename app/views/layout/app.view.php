<?php

/**
 * @var Sys\View\ViewEngineInterface $v
 * @var string                       $slot
 */

?>

<!doctype html>
<html lang="<?= config()->get('locale.default'); ?>" class="h-full">
<head>
    <meta charset="UTF-8">
    <meta
        name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"
    >
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title><?= $v->persist('title') ?: config()->get('app.name', 'Vanilla PHP'); ?></title>

    <link href="./styles.css" rel="stylesheet">
</head>
<body class="antialiased font-sans h-full">
    <?= $slot; ?>
</body>
</html>
