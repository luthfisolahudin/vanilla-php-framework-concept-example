<?php

declare(strict_types=1);

use App\User\Auth\Controllers\LoginActionController;
use App\User\Auth\Controllers\LoginController;
use App\User\Auth\Controllers\RegisterActionController;
use App\User\Auth\Controllers\RegisterController;
use App\User\Home\Controllers\HomeController;
use App\User\Home\Controllers\IntendedHomeController;
use Sys\Http\Middleware\AuthenticatedMiddleware;
use Sys\Http\Middleware\GuestMiddleware;

router()->get('/', IntendedHomeController::class, [AuthenticatedMiddleware::class]);
router()->get('/home', HomeController::class, [AuthenticatedMiddleware::class]);
router()->get('/register', RegisterController::class, [GuestMiddleware::class]);
router()->post('/register', RegisterActionController::class, [GuestMiddleware::class]);
router()->get('/login', LoginController::class, [GuestMiddleware::class]);
router()->post('/login', LoginActionController::class, [GuestMiddleware::class]);
