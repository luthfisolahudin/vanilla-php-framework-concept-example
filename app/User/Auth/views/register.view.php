<?php

/**
 * @var Sys\View\ViewEngineInterface $v
 */

$v->extends('layout/app');

?>

<main class="flex h-full">
    <div class="container mx-auto">
        <form action="/register" method="post">
            <div>
                <label for="username">Username</label>
                <input type="text" name="username" id="username">
            </div>
            <div>
                <label for="password">Password</label>
                <input type="password" name="password" id="password">
            </div>
            <div>
                <label for="password-confirm">Password</label>
                <input type="password" name="password_confirm" id="password-confirm">
            </div>
            <div>
                <button type="submit">Login</button>
            </div>
        </form>
    </div>
    <div class="container mx-auto self-end flex">
        <p class="mx-auto">Made with $</p>
    </div>
</main>
