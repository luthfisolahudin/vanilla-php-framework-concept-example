<?php

/**
 * @var Sys\View\ViewEngineInterface $v
 */

$v->extends('layout/app');

?>

<main class="container mx-auto h-full flex flex-col gap-2 items-center justify-center p-2">
    <?php if (session()->get('action.invalid')): ?>
        <div class="rounded-md bg-red-50 py-2 px-4 w-full max-w-[45ch]" role="alert">
            <p class="text-sm text-red-800 font-medium">
                Kombinasi username dan password salah
            </p>
        </div>
    <?php endif; ?>

    <form action="/login" method="post" class="border border-gray-300 rounded-md p-4 w-full max-w-[45ch]">
        <div class="mb-2">
            <label for="username" class="font-medium text-sm">Username</label>
            <input type="text" name="username" id="username" class="block w-full border border-gray-300 rounded-md pb-1 pt-1.5 px-2 focus:outline-blue-300 text-sm">
        </div>
        <div class="mb-2">
            <label for="password" class="font-medium text-sm">Password</label>
            <input type="password" name="password" id="password" class="block w-full border border-gray-300 rounded-md pb-1 pt-1.5 px-2 focus:outline-blue-300 text-sm">
        </div>
        <button type="submit" class="block w-full border border-gray-300 py-1.5 px-2 font-medium text-sm rounded-md mb-2 focus:outline-blue-300">
            Login
        </button>

        <hr class="my-2">

        <div class="text-sm text-center">
            Belum mempunyai akun?

            <a href="/register" class="underline underline-offset-4 decoration-gray-400 decoration-dotted focus:outline-blue-300 focus:decoration-blue-300 hover:decoration-blue-300 hover:decoration-solid hover:decoration-2">
                Daftar
            </a>
        </div>
    </form>
</main>
