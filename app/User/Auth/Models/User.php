<?php

declare(strict_types=1);

namespace App\User\Auth\Models;

use Sys\Http\Auth\User as BaseUser;

class User extends BaseUser
{
    protected string $email;

    public static function fromDatabase(array $values): static
    {
        $user = new static();

        $user->username = $values['username'];
        $user->email = $values['email'];

        return $user;
    }
}
