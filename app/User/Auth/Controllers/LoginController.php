<?php

namespace App\User\Auth\Controllers;

use Sys\Http\Controller\ControllerInterface;

class LoginController implements ControllerInterface
{
    public function __invoke(): string
    {
        return view()->render('user/auth:login');
    }
}
