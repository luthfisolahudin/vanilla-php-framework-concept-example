<?php

declare(strict_types=1);

namespace App\User\Home\Controllers;

class IntendedHomeController
{
    public function __invoke(): void
    {
        request()->redirect('/home');
    }
}
