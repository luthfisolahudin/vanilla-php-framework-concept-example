<?php

declare(strict_types=1);

namespace App\User\Home\Controllers;

class HomeController
{
    public function __invoke()
    {
        return view()->render('user/home:index');
    }
}
