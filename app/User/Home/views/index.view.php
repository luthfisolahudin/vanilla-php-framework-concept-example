<?php

/**
 * @var Sys\View\ViewEngineInterface $v
 * @var object[] $notes
 */

$v->extends('layout/app');
$v->persist('title', 'Home');

?>

<main class="container mx-auto">
    <?php if (empty($notes)): ?>
        <p>Kosong</p>
    <?php else: ?>
        <ul>
            <?php foreach ($notes as $note): ?>
                <li><?= $note ?></li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
</main>
